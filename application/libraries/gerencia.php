<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gerencia
{
  protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function encriptar_clave( $clave )
	{
		return $this->ci->encrypt->encode( $clave );
	}

	

}

/* End of file gerencia.php */
/* Location: ./application/libraries/gerencia.php */
