<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administracion extends CI_Controller {
	

	public function __construct()
	{
		parent::__construct();
		$this->load->model('gerencia_model');
		$this->_init();
	}

	/**
	 * inicializacion de parametro para este controlador
	 * @return [type] [description]
	 */
	private function _init()
	{
		$this->output->set_template('sublime');

		// los archivos de javascript personalizados se cargaran de la siguiente manera
		//$this->load->js('assets/themes/default/hero_files/bootstrap-collapse.js');
		

	}

	public function index()
	{
		$this->load->js('assets/themes/sublime/plugins/parsley.min.js');
		$this->load->view('administracion/registro');

	}

	private function _nuevo_usuario()
	{
		# code...
	}

}

/* End of file administracion.php */
/* Location: ./application/controllers/administracion.php */