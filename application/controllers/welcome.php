<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->_init();
	}

	/**
	 * inicializacion de parametro para este controlador
	 * @return [type] [description]
	 */
	private function _init()
	{
		$this->output->set_template('sublime');

		// los archivos de javascript personalizados se cargaran de la siguiente manera
		//$this->load->js('assets/themes/default/hero_files/bootstrap-collapse.js');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["dato"] = $this->encrypt->encode("alan");
		$data["dato_limpio"] = $this->encrypt->decode("XPFY+GkF9UCaTMXi9qHAj1CsUtIO9q8DT0qkxruLxpul73VwCyoF0hEWHS7Oaq0lYYKimQJMuEjmtb0AAnmbdQ==");
		$this->load->view('welcome_message', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */