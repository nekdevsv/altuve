<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gerencia_model extends CI_Model
{
    
    public $id_usuario_sistema, $nombre_usuario, $apellido_usuario, $alias_usuario, $clave_usuario, $usuario_activo;
    
    public function __construct()
    {
        parent::__construct();
        $this->id_usuario_sistema = new stdClass();
        $this->nombre_usuario     = new stdClass();
        $this->apellido_usuario   = new stdClass();
        $this->alias_usuario      = new stdClass();
        $this->clave_usuario      = new stdClass();
        $this->usuario_activo     = false;
        $this->load->library('gerencia');
    }
    
    public function nuevo_usuario()
    {

        $data = array(
            'nombre_usuario' => $this->nombre_usuario,
            'apellido_usuario' => $this->apellido_usuario,
            'alias_usuario' => $this->alias_usuario,
            'clave_usuario' => $this->gerencia->encriptar_clave($this->clave_usuario),
            'usuario_activo' => true
        );
        
        $resultado = $this->db->insert('usuario_sistema', $data);
        
        return (is_object($resultado) && $resultado->num_rows() > 0) ? $resultado : false;
    }
    
    
    
}

/* End of file gerencia_model.php */
/* Location: ./application/models/gerencia_model.php */