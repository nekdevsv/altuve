<div class="col-md-12">
	<section class="panel">
		<header class="panel-heading">
			Nuevo usuario
		</header>
		<div class="panel-body">
			<form role="form" class="parsley-form" data-parsley-validate>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Nombres</label>
							<div>
								<input type="text" class="form-control" name="firstname" data-parsley-required="true" data-parsley-trigger="change" placeholder="First Name">
							</div>
						</div>
						<div class="form-group">
							<label>Apellidos</label>
							<div>
								<input type="text" class="form-control" name="lastname" data-parsley-required="true" data-parsley-trigger="change" placeholder="Last Name">
							</div>
						</div>
						<div class="form-group">
							<label>Usuario</label>
							<div>
								<input type="text" class="form-control" name="country" data-parsley-required="true" data-parsley-trigger="change" placeholder="Country">
							</div>
						</div>
						<div class="form-group">
							<label>Contrase&ntilde;a</label>
							<div>
								<input type="text" class="form-control" name="state" data-parsley-required="true" data-parsley-trigger="change" placeholder="State/Province">
							</div>
						</div>
						<div class="form-group">
							<label>Confirmar contrase&ntilde;a</label>
							<div>
								<input type="text" class="form-control" name="city" data-parsley-required="true" data-parsley-trigger="change" placeholder="City">
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group text-center">
							<label></label>
							<div>
								<button class="btn btn-primary btn-block btn-lg btn-parsley">Guardar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		
	</div>
	<p>Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</section>
</div>